package main;

import errors.FatalMessageException;
import interp.InterpError;
import transpiler.TranspileProject;
import transpiler.TranspilerError;

public class Main {
  private static String inputFile = null;

  public static void main(String[] args) {
    parseParameters(args);
    TranspileProject transpiler = null;

    if (inputFile == null) {
      System.out.println("cxx_transpiler: no input ast.bson file specified");
      System.exit(1);
    }
    transpiler = new TranspileProject(inputFile);
    try {
      transpiler.run();
    } catch (FatalMessageException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      System.out.println(transpiler.project.errorReport());
    } catch (InterpError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (TranspilerError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static void parseParameters(String[] args) {
    for (int i = 0; i < args.length; i++) {
      if (args[i].charAt(0) == '-') {
        System.err.println("unexpected option: " + args[i]);
        System.exit(1);
      } else if (inputFile != null) {
        System.err.println("too many files specified");
        System.exit(1);
      } else {
        inputFile = args[i];
      }
    }
  }
}
