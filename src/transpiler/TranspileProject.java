package transpiler;

import ast.Ast;
import ast.SerializationError;
import errors.FatalMessageException;
import interp.InterpError;
import java.io.IOException;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import threading.ThreadManager;

public class TranspileProject {

  private final String astFname;
  public Project project = null;

  public TranspileProject(String astFname) {
    this.astFname = astFname;
  }

  @SuppressWarnings("resource")
  public void run()
      throws FatalMessageException, InterpError, InterruptedException, TranspilerError {
    System.out.println("Running cxx_transpiler: " + astFname);

    try {
      project = Project.deserializeFromFile(astFname, "build-jsmnt-cxx");
    } catch (IOException | SerializationError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return;
    }

    ThreadManager threadManager = new ThreadManager(1);

    project.linkModules(threadManager);

    threadManager.stop();

    project = project.linkRequiredProvidedFunctions("C++").get();

    project = project.resolveUserTypes().get();

    project = project.normalizeTypes(false);

    project = project.removeNonModuleImports();

    project = project.insertStartFunction();

    project = project.liftClassDeclarations();

    // run renaming to remove shadowing
    // project = project.renameIds().get();

    // project = project.normalizeTypes(false);

    // project = project.renameTypes(); //like renameIds but for types

    project.typecheck(); // TODO remove, here just for debugging

    // make sure we have no parameterized types (so no templates)
    project = project.resolveGenerics().get();

    project.typecheck(); // TODO remove, here just for debugging

    project = project.moveInitsToConstructors();

    // project.typecheck(); // TODO remove, here just for debugging

    project = project.renameIds().get();

    project = project.insertDestructorCalls();

    project = project.toAnf(Ast.AnfTune.T0).get();

    project = project.resolveExceptions().get();

    project = project.renameIds().get();

    project = project.normalizeTypes(false);

    project = project.renameTypes();

    project.typecheck();

    project = project.resolveVtables().get();

    project.typecheck();

    if (project.hitError()) {
      System.out.println(project.errorReport());
      return;
    }

    try {
      transpileProject(project);
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }
  }

  /*
   * Split up each module into a header and source file. Then 'import std' becomes
   * '#include "std.hpp";' This requires no real knowledge of the other modules,
   * since every module path will be unique and each program only needs to handle
   * the import and submod stuff.
   */
  private void transpileProject(Project project)
      throws IOException, FatalMessageException, TranspilerError {
    // collect custom_to_string
    CxxBuiltinFunctionOverloads builtinOverloads = new CxxBuiltinFunctionOverloads(
        new CxxBuiltinGenerator("cxx_builtin"));
    builtinOverloads.initBuiltinBasic();
    builtinOverloads.populate(project);

    // generate builtin.hpp/cpp
    builtinOverloads.builtinGenerator.generate();

    transpileProjectHelper(project, builtinOverloads);
  }

  private void transpileProjectHelper(Project project, CxxBuiltinFunctionOverloads builtinOverloads)
      throws IOException, FatalMessageException, TranspilerError {

    for (Project lib : project.getBuildLibs()) {
      transpileProjectHelper(lib, builtinOverloads);
    }

    for (ModuleEndpoint mod : project.getSources()) {
      mod.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) mod.getHandle();
      TranspileProgram tp = new TranspileProgram(project, moduleHandle.moduleBuildObj.program,
          moduleHandle.moduleBuildObj.effectiveImports.applyTransforms(project.getAuditTrail(),
              mod.msgState),
          builtinOverloads);
      tp.transpileProgram();
    }
  }
}
