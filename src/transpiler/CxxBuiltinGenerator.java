package transpiler;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import transpile.BuiltinFunctionOverloads.FunctionOverloadReturn;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import transpile.BuiltinGenerator;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.Float32Type;
import typecheck.Float64Type;
import typecheck.IntType;
import typecheck.StringType;

public class CxxBuiltinGenerator extends BuiltinGenerator {

  public static final String HPP_NAME = "builtin.hpp";
  public static final String CPP_NAME = "builtin.cpp";
  public static final String OVERLOAD_CUSTOM_TO_STRING = "custom_to_string";

  private String hppResult = "";
  private String cppResult = "";

  public CxxBuiltinGenerator(String namespace) {
    super(namespace);
  }

  public void generate() throws IOException {
    generatePrefix();
    generateMisc();
    generateCustomToString();
    generateSuffix();

    String dirPath = "build-cxx/" + namespace + "/";
    outputToFile(hppResult, dirPath + HPP_NAME);
    outputToFile(cppResult, dirPath + CPP_NAME);
  }

  private void outputToFile(String data, String filepath) throws IOException {
    FileOutputStream stream = TranspileProgram.openFile(filepath);
    stream.write(data.getBytes());
    stream.close();
  }

  private void generatePrefix() {
    hppResult += "#ifndef _JASMINT_CXX_TRANSPILER_BUILTIN_H_                               \n"
        + "#define _JASMINT_CXX_TRANSPILER_BUILTIN_H_                                      \n"
        + "                                                                                \n"
        + "#include <iostream>                                                             \n"
        + "#include <string>                                                               \n"
        + "#include <sstream>                                                              \n"
        + "#include <vector>                                                               \n"
        + "#include <map>                                                                  \n"
        + "#include <iomanip>                                                              \n"
        + "#include <chrono>                                                               \n"
        + "#include <cstdint>                                                              \n"
        + "                                                                                \n"
        + "namespace " + namespace + " {\n";
    cppResult += "#include \"../cxx_builtin/builtin.hpp\"\nnamespace cxx_builtin {\n";
  }

  private void generateSuffix() {
    hppResult += "}\n#endif";
    cppResult += "}";
  }

  private void generateCustomToString() {
    Set<NamespaceName> usingNamespaces = new HashSet<>();
    usingNamespaces.add(namespaceName);

    FunctionOverloadReturn tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, StringType.Create()).get();
    String overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(const std::string& str);\n";
    cppResult += "std::string " + overload + "(const std::string& str) {\n  return str;\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, BoolType.Create()).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(bool val);\n";
    cppResult += "std::string " + overload
        + "(bool val) {\n  return val ? \"true\" : \"false\";\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, CharType.Create()).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(char val);\n";
    cppResult += "std::string " + overload + "(char val) {\n  return std::string(1, val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, Float32Type.Create()).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(float val);\n";
    cppResult += "std::string " + overload + "(float val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, Float64Type.Create()).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(double val);\n";
    cppResult += "std::string " + overload + "(double val) {                      \n"
        + "   std::string tmp = std::to_string(val);                              \n"
        + "   //only leave 2.0 or 2.1                                             \n"
        + "   int i;                                                              \n"
        + "   for (i = tmp.size() - 1; i >= 0; i--) {                             \n"
        + "      if (tmp.at(i) != '0') {                                          \n"
        + "         if (tmp.at(i) == '.') {                                       \n"
        + "            i++;                                                       \n"
        + "         }                                                             \n"
        + "         break;                                                        \n"
        + "      }                                                                \n"
        + "   }                                                                   \n"
        + "   return tmp.substr(0, i + 1);                                        \n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 8)).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(uint8_t val);\n";
    cppResult += "std::string " + overload + "(uint8_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 16))
        .get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(uint16_t val);\n";
    cppResult += "std::string " + overload + "(uint16_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 32))
        .get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(uint32_t val);\n";
    cppResult += "std::string " + overload + "(uint32_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(false, 64))
        .get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(uint64_t val);\n";
    cppResult += "std::string " + overload + "(uint64_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 8)).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(int8_t val);\n";
    cppResult += "std::string " + overload + "(int8_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 16)).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(int16_t val);\n";
    cppResult += "std::string " + overload + "(int16_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 32)).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(int32_t val);\n";
    cppResult += "std::string " + overload + "(int32_t val) {\n  return std::to_string(val);\n}\n";

    tmpOverload = getBuiltinOverloads()
        .lookupOverload(usingNamespaces, OVERLOAD_CUSTOM_TO_STRING, IntType.Create(true, 64)).get();
    overload = tmpOverload.getNewName().a;
    hppResult += "std::string " + overload + "(int64_t val);\n";
    cppResult += "std::string " + overload + "(int64_t val) {\n  return std::to_string(val);\n}\n";
  }

  private void generateMisc() {
    hppResult += "double currentUnixEpochTime();\n";
    cppResult += "double currentUnixEpochTime() {                                          \n"
        + "   auto a = std::chrono::system_clock::now();                                   \n"
        + "   time_t b = std::chrono::system_clock::to_time_t(a);                          \n"
        + "   return (double) b;;                                                          \n"
        + "}\n";

    hppResult += "void custom_print(const std::string& out);\n";
    cppResult += "void custom_print(const std::string& out) {                              \n"
        + "   std::cout << out << std::flush;                                              \n"
        + "}\n";

    hppResult += "template<typename T>                                                     \n"
        + "T custom_remove(std::vector<T>& vec, int i) {                                   \n"
        + "   T tmp = vec.at(i);                                                           \n"
        + "   vec.erase(vec.begin() + i);                                                  \n"
        + "   return tmp;                                                                  \n"
        + "}                                                                               \n"
        + "                                                                                \n"
        + "template<typename T>                                                            \n"
        + "void custom_insert(std::vector<T>& vec, int i, T item) {                        \n"
        + "   vec.insert(vec.begin() + i, item);                                           \n"
        + "}                                                                               \n"
        + "                                                                                \n"
        + "template<typename K, typename V>                                                \n"
        + "V custom_remove(std::map<K, V>& obj, const K& key) {                            \n"
        + "   V tmp = obj.at(key);                                                         \n"
        + "   obj.erase(key);                                                              \n"
        + "   return tmp;                                                                  \n"
        + "}                                                                               \n"
        + "                                                                                \n"
        + "template<typename K, typename V>                                                \n"
        + "void custom_insert(std::map<K, V>& obj, K key, V value) {                       \n"
        + "   obj.insert(std::pair<K, V>(key, value));                                     \n"
        + "}\n";
  }
}
