package transpiler;

import ast.AbstractExpression;
import ast.AbstractLvalue;
import ast.AbstractStatement;
import ast.ArrayInitExpression;
import ast.AssignmentStatement;
import ast.Ast;
import ast.BinaryExpression;
import ast.BlockStatement;
import ast.BoolExpression;
import ast.BracketExpression;
import ast.CastExpression;
import ast.CharExpression;
import ast.ClassDeclaration;
import ast.ClassDeclaration.ClassLine;
import ast.ConditionalStatement;
import ast.DeclarationStatement;
import ast.DeleteStatement;
import ast.DereferenceExpression;
import ast.DotExpression;
import ast.EnumDeclaration;
import ast.Expression;
import ast.ExpressionStatement;
import ast.FfiExpression;
import ast.FfiStatement;
import ast.FloatExpression;
import ast.ForStatement;
import ast.Function;
import ast.Function.MetaType;
import ast.FunctionCallExpression;
import ast.FunctionCallStatement;
import ast.IdentifierExpression;
import ast.ImportStatement;
import ast.InstanceofExpression;
import ast.IntegerExpression;
import ast.LambdaExpression;
import ast.LeftUnaryOpExpression;
import ast.Lvalue;
import ast.LvalueBracket;
import ast.LvalueDereference;
import ast.LvalueDot;
import ast.LvalueFfi;
import ast.LvalueId;
import ast.MapInitExpression;
import ast.MultiExpression;
import ast.MultiLvalue;
import ast.NewClassInstanceExpression;
import ast.NewExpression;
import ast.Program;
import ast.ReferenceExpression;
import ast.ReturnStatement;
import ast.SizeofExpression;
import ast.Statement;
import ast.StaticClassIdentifierExpression;
import ast.StringExpression;
import ast.SubmodStatement;
import ast.VtableAccessExpression;
import ast.WhileStatement;
import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditRenameTypes;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.Nullable;
import import_mgmt.EffectiveImport;
import import_mgmt.EffectiveImport.EffectiveImportType;
import import_mgmt.EffectiveImports;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import mbo.Renamings.RenamingIdKey;
import mbo.Vtables.ClassVtable;
import mbo.Vtables.VtableEntry;
import multifile.JsmntGlobal;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import transpile.BuiltinFunctionOverloads.FunctionOverloadReturn;
import transpile.BuiltinFunctionOverloads.NamespaceName;
import typecheck.AbstractType;
import typecheck.ArrayType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.FunctionType;
import typecheck.FunctionType.LambdaStatus;
import typecheck.ImportType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.ReferenceType;
import typecheck.ReferencedType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UserDeclType;
import typecheck.UserInstanceType;
import typecheck.VoidType;
import util.Pair;

public class TranspileProgram {
  public static final String LANG = "CXX";

  private final Project project;
  private final Program program;
  private final String moduleNamespace;
  private final String cppFname;
  private final String hppFname;
  private final String incHppFname;
  private final EffectiveImports effectiveImports;
  private final CxxBuiltinFunctionOverloads builtinOverloads;

  private static final String INCREASE_INDENT = "  ";
  private String indent = "";

  private String topLevelFuncsDecls = "";
  private String forwardClassDecls = "";
  private List<String> definedTypes = new LinkedList<String>();
  // List<Pair<className(incomplete type), declaration>
  private List<Pair<String, String>> deferredStatics = new LinkedList<Pair<String, String>>();
  private String hppImports = "";
  private String progName;

  private Set<String> hashFunctions = new HashSet<>();

  private Set<NamespaceName> namespaces = new HashSet<>();
  private String usingNamespaces = "";

  private String generatedTemplatesCpp = "";

  private ClassDeclType rootObjType = null;
  private EnumDeclType cmpResultType = null;
  private String cmpEqId = "";

  public TranspileProgram(Project project, Program program, EffectiveImports effectiveImports,
      CxxBuiltinFunctionOverloads builtinOverloads) {
    this.project = project;
    this.program = program;
    String projectName = project.dirPath();
    this.progName = program.moduleStatement.fullType.toString();
    this.cppFname = "build-cxx/" + projectName + "/" + progName + ".cpp";
    this.incHppFname = "../" + projectName + "/" + progName + ".hpp";
    this.hppFname = "build-cxx/" + projectName + "/" + progName + ".hpp";
    this.effectiveImports = effectiveImports;
    this.builtinOverloads = builtinOverloads.extend();

    moduleNamespace = projectName + "::"
        + program.moduleStatement.fullType.toString().replace(".", "::");
  }

  public static FileOutputStream openFile(String fname) throws IOException {
    File outFile = new File(fname).getAbsoluteFile();
    if (!outFile.exists()) {
      String dir = outFile.getParent();
      if (dir != null) {
        new File(dir).mkdirs();
      }
      outFile.createNewFile();
    }

    System.out.println("  -> " + outFile);

    FileOutputStream fout = null;
    try {
      fout = new FileOutputStream(outFile);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalArgumentException(e.getMessage());
    }
    return fout;
  }

  private void getJsmntGlobalRenamings() throws FatalMessageException, TranspilerError {
    Nullable<ModuleEndpoint> modEp = project.lookupModule(JsmntGlobal.modType,
        new AuditTrailLite());
    if (modEp.isNull()) {
      throw new TranspilerError("unable to find jsmnt_global");
    }
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();

    Nullable<AuditEntry> globalRenames = moduleHandle.moduleBuildObj.auditTrail
        .getCombined(AuditEntryType.RENAME_TYPES);
    if (globalRenames.isNull()) {
      throw new TranspilerError("unable to find type renames");
    }
    AuditRenameTypes globalRenaming = (AuditRenameTypes) globalRenames.get();
    Type tmp = globalRenaming.typeRenamings
        .get(new EnumDeclType(Nullable.of(JsmntGlobal.modType), "CmpResult"));
    if (tmp == null) {
      throw new TranspilerError("unable to find cmp enum in type renames");
    }
    this.cmpResultType = (EnumDeclType) tmp;

    tmp = globalRenaming.typeRenamings
        .get(new ClassDeclType(Nullable.of(JsmntGlobal.modType), "Obj"));
    if (tmp == null) {
      throw new TranspilerError("unable to find Obj class in type renames");
    }
    this.rootObjType = (ClassDeclType) tmp;

    // now look up EQ
    RenamingIdKey idKey = new RenamingIdKey(
        Nullable.of(new EnumDeclType(Nullable.of(JsmntGlobal.modType), "CmpResult")),
        Nullable.empty(), "EQ");
    RenamingIdKey newIdKey = idKey.applyTransforms(moduleHandle.moduleBuildObj.auditTrail);

    this.cmpEqId = newIdKey.id;
  }

  public void transpileProgram() throws IOException, FatalMessageException, TranspilerError {
    FileOutputStream out = openFile(cppFname);
    FileOutputStream hpp = openFile(hppFname);

    // find jsmnt global renames
    getJsmntGlobalRenamings();

    /* add common imports. In future can check for which ones are actually needed */
    hpp.write(
        ("#pragma once\n" + "#include <iostream>\n" + "#include <string>\n" + "#include <map>\n"
            + "#include <vector>\n" + "#include <functional>\n" + "#include <tuple>\n" + "\n")
                .getBytes());

    String cppResult = "#include \"" + incHppFname + "\"" + "\n";

    String hppResult = "";

    // run through program lines and collect all declarations (class, fn, var).
    // transpile those first
    List<Ast> declarations = new LinkedList<Ast>();
    List<Ast> body = new LinkedList<Ast>();

    for (Ast ast : program.getLines()) {
      if (ast instanceof ClassDeclaration || ast instanceof Function || ast instanceof FfiStatement
          || ast instanceof EnumDeclaration) {
        declarations.add(ast);
      } else {
        body.add(ast);
      }
    }

    // find our module (check our effective imports to find which project each
    // module comes from)
    Nullable<ModuleEndpoint> ourModEp = project.lookupModule(program.moduleStatement.fullType,
        project.getAuditTrail());
    if (ourModEp.isNull()) {
      // scream about it
      throw new TranspilerError("failed to find our own module endpoint");
    }

    // transpile imports
    for (ImportStatement imp : program.imports) {
      Nullable<ModuleEndpoint> modEp = project.lookupModule(imp.target, project.getAuditTrail());
      if (modEp.isNull()) {
        // scream about it
        throw new TranspilerError("failed to find module endpoint for import: " + imp.target);
      }
      String projectDir = modEp.get().project.dirPath();
      String importPath = imp.target.toString();
      String fullPath = projectDir + "/" + importPath;

      hppResult += "#include \"../" + fullPath + ".hpp\"\n";
    }

    // add built-in functions (can't use any built-in stuff in the headers)
    cppResult += addBuiltInFunctions();

    // transpile declarations first, in global scope
    // transpile everything else into main
    try {

      Set<EffectiveImportType> filterImpTypes = new HashSet<>();
      filterImpTypes.add(EffectiveImportType.LITERAL_IMPORT);
      filterImpTypes.add(EffectiveImportType.INHERITED_IMPORT);
      filterImpTypes.add(EffectiveImportType.IMPLIED_IMPORT);

      namespaces.add(builtinOverloads.builtinGenerator.namespaceName);

      for (Entry<ImportType, EffectiveImport> entry : effectiveImports
          .getImportMap(true, filterImpTypes).entrySet()) {
        NamespaceName namespace = entry.getValue().getNamespaceName();
        String namespaceTmp = entry.getValue().withinProject.get().toString();
        if (namespaces.contains(namespace)) {
          continue;
        }
        namespaces.add(namespace);

        String newUsing = "using namespace " + namespaceTmp + ";\n";
        if (!usingNamespaces.contains(newUsing)) {
          usingNamespaces += newUsing;
        }

        String projectDir = entry.getValue().withinProject.get().toString();
        String importPath = entry.getKey().toString();
        String fullPath = projectDir + "/" + importPath;
        cppResult = "#include \"../" + fullPath + ".hpp\"\n" + cppResult;
      }

      String preResult = cppResult + "\nusing namespace " + moduleNamespace + ";\n";
      preResult += "using namespace " + builtinOverloads.builtinGenerator.namespace + ";\n";
      preResult += usingNamespaces;

      Pair<String, String> cppHpp = transpileGlobalDeclarations(declarations);
      String tmpMain = transpileMain(program);

      hppResult += hppImports;

      hppResult += "namespace " + moduleNamespace + " {\n";

      hppResult += forwardClassDecls + "\n";

      String tmpGDecl = cppHpp.a;

      String result = "";

      hppResult += topLevelFuncsDecls + "\n";
      hppResult += "};\n";
      hppResult += cppHpp.b;

      // add hash functions
      if (!hashFunctions.isEmpty()) {
        hppResult += "namespace std {\n";
        for (String fn : hashFunctions) {
          hppResult += fn + "\n";
        }
        hppResult += "}\n";
      }

      // import submods by including their headers at the end
      for (SubmodStatement submod : program.submods) {
        hppResult += "#include \"" + progName + "." + submod.absSubmodPath.name.toString()
            + ".hpp\"\n";
      }

      String finalResult = preResult + generatedTemplatesCpp + result;

      finalResult += tmpGDecl + "\n";
      finalResult += tmpMain;
      out.write(finalResult.getBytes());
      hpp.write(hppResult.getBytes());
    } catch (TranspilerError |

        FatalMessageException e) {
      e.printStackTrace();
      return;
    }

    try {
      out.close();
      hpp.close();
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }
  }

  private String addBuiltInFunctions() {
    String result = "";

    result += "#include \"../cxx_builtin/builtin.hpp\"\n\n";

    return result;
  }

  private Pair<String, String> transpileGlobalDeclarations(List<Ast> declarations)
      throws TranspilerError, FatalMessageException {
    String out = "";
    String hppOut = "";

    // transpile all enums first, since they have no dependency on anything else
    for (Ast ast : declarations) {
      if (ast instanceof EnumDeclaration) {
        hppOut += transpileEnumDeclaration((EnumDeclaration) ast).b + "\n";
      }
    }

    out += "namespace " + moduleNamespace + "{\n";

    for (Ast ast : declarations) {
      if (ast instanceof ClassDeclaration) {
        Pair<String, String> tmp = transpileClassDeclaration((ClassDeclaration) ast);
        out += tmp.a + "\n";
        hppOut += tmp.b + "\n";
      } else if (ast instanceof Statement) {

        out += transpileStatement((Statement) ast);
        if (!(ast instanceof FfiStatement)) {
          out += ";";
        }

      } else if (ast instanceof Function) {
        throw new IllegalArgumentException("function is a statement");
      } else if (ast instanceof EnumDeclaration) {
        continue;
      }
    }

    out += "\n}\n";
    return new Pair<String, String>(out, hppOut);
  }

  private Pair<String, String> transpileEnumDeclaration(EnumDeclaration enumDecl) {
    String result = "";
    String hppResult = "";

    definedTypes.add(enumDecl.enumType.name);

    hppResult += "namespace " + moduleNamespace + " {\n";
    hppResult += usingNamespaces;
    hppResult += "enum class " + enumDecl.enumType.name + "{";
    boolean first = true;
    for (String id : enumDecl.enumIds) {
      if (!first) {
        hppResult += ", ";
      }
      first = false;
      hppResult += id;
    }
    hppResult += "};";
    hppResult += "};";

    return new Pair<>(result, hppResult);
  }

  private Pair<String, String> transpileClassDeclaration(ClassDeclaration cdecl)
      throws TranspilerError, FatalMessageException {
    String result = "";
    String hppResult = "";

    // custom_to_string forward declaration
    hppResult += "namespace " + moduleNamespace + " {\n";
    hppResult += usingNamespaces;

    // lookup the overload names
    FunctionOverloadReturn tmpStackOverload = builtinOverloads.lookupOverload(namespaces,
        CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, cdecl.fullType.asDeclType()).get();
    FunctionOverloadReturn tmpPtrOverload = builtinOverloads.lookupOverload(namespaces,
        CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, new ReferenceType(cdecl.fullType)).get();
    String stackOverload = tmpStackOverload.getNewName().a;
    String ptrOverload = tmpPtrOverload.getNewName().a;

    hppResult += indent + "std::string " + stackOverload + "(const " + cdecl.name + "& item);\n";
    hppResult += indent + "std::string " + ptrOverload + "(const " + cdecl.name + "* item);\n";

    String customToStrFn = indent + "std::string " + stackOverload + "(const " + cdecl.name
        + "& item) {\n" + indent + INCREASE_INDENT + "(void)item;\n" + indent + INCREASE_INDENT
        + "return \"<class instance>\";\n" + indent + "}\n";

    String customRefToStrFn = indent + "std::string " + ptrOverload + "(const " + cdecl.name
        + "* item) {\n" + indent + INCREASE_INDENT + "(void)item;\n" + indent + INCREASE_INDENT
        + "return \"<reference::\" + item->refStr + \">\";\n" + indent + "}\n";

    // handle generics (these should be removed)
    if (cdecl.genericTypeNames.size() > 0) {
      throw new TranspilerError("All generics should be removed: " + cdecl);
    }
    hppResult += indent + "class " + cdecl.name;
    if (cdecl.parent.isNotNull()) {
      hppResult += " : public " + transpileType(cdecl.parent.get());
    }

    definedTypes.add(cdecl.name);

    // add forward declaration
    forwardClassDecls += "class " + cdecl.name + ";\n";

    String saveIndent = indent;
    hppResult += " {\n" + indent + "public:\n";
    indent += INCREASE_INDENT;

    if (cdecl.parent.isNull()) {
      // add refStr as const string (could be enum) if no parent, along with
      // setRefStr()
      hppResult += indent + "std::string refStr = \"" + transpileType(cdecl.fullType) + "\";\n";
    }
    String retTyStr = transpileType(new ReferenceType(cdecl.fullType));
    hppResult += indent + retTyStr + " setRefStr(const std::string& str);\n";
    String implementSetRefStr = retTyStr + " " + cdecl.name
        + "::setRefStr(const std::string& str) {\n" + indent + INCREASE_INDENT + "refStr = str;\n"
        + indent + INCREASE_INDENT + "return this;\n}\n";

    // collect declarations and do value assignments in real constructor
    String realConstructor = indent + cdecl.name + "();\n";
    String implementRealConstructor = cdecl.name + "::" + cdecl.name + "() {\n";

    hppResult += realConstructor;

    String collectStaticAssignments = "";
    for (ClassLine cline : cdecl.lines) {
      if (cline.ast instanceof DeclarationStatement) {
        DeclarationStatement decl = (DeclarationStatement) cline.ast;
        hppResult += indent;
        if (decl.getIsStatic()) {
          hppResult += "static " + transpileType(decl.type) + " " + decl.name + ";\n";

          String tmp = transpileType(decl.type) + " " + cdecl.name + "::" + decl.name;
          if (decl.getValue().isNotNull()) {
            tmp += " = " + transpileExpression(decl.getValue().get());
          }
          tmp += ";\n";

          if (decl.type instanceof UserInstanceType) {
            UserInstanceType uty = (UserInstanceType) decl.type;
            if (!definedTypes.contains(uty.name)) {
              deferredStatics.add(new Pair<String, String>(uty.name, tmp));
            } else {
              collectStaticAssignments += tmp;
            }
          } else {
            collectStaticAssignments += tmp;
          }
        } else {
          hppResult += transpileDeclarationStatement(decl) + ";\n";
          if (decl.getValue().isNotNull()) {
            implementRealConstructor += indent + INCREASE_INDENT + decl.name + " = "
                + transpileExpression(decl.getValue().get()) + ";\n";
          }
        }
      }
    }
    result += implementRealConstructor + "\n" + indent + "}\n";
    result += implementSetRefStr;
    // transpile functions
    String hashFnName = "";
    String cmpFnName = "";
    for (ClassLine cline : cdecl.lines) {
      if (cline.ast instanceof Function) {
        Function fn = (Function) cline.ast;

        IdentifierExpression selfExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
            new LinkedList<>(), "this");
        selfExpr.setDeterminedType(new ReferenceType(cdecl.fullType));
        // DereferenceExpression derefExpr = new DereferenceExpression(-1, -1,
        // selfExpr);
        // derefExpr.setDeterminedType(cdecl.fullType);

        if (fn.metaType == MetaType.CONSTRUCTOR) {
          // add parent constructor call
          if (fn.parentCall.isNotNull()) {
            // after the rename, the parentCall expression will be a regular identifier, not
            // "parent"
            if (fn.parentCall.get().prefix.isNotNull()) {
              throw new IllegalArgumentException();
            }
            FunctionCallExpression fnCallExpr = new FunctionCallExpression(Nullable.of(selfExpr),
                fn.parentCall.get().expression, fn.parentCall.get().arguments,
                fn.parentCall.get().originalName);
            fnCallExpr.setDeterminedType(VoidType.Create());
            FunctionCallStatement pFnCall = new FunctionCallStatement(fnCallExpr);
            pFnCall.setDeterminedType(VoidType.Create());

            fn.getBody().get().statements.add(0, pFnCall);
          }

          // replace any 'return;' statements with 'return selfExpr;'
          ReturnStatement returnSelf = new ReturnStatement(Nullable.of(selfExpr));
          BlockStatement fnBody = fn.getBody().get();
          fnBody = (BlockStatement) fnBody.replace(new ReturnStatement(Nullable.empty()),
              returnSelf);
          fnBody.statements.add(returnSelf);

          // change the return type
          Function tmp = new Function(fn.lineNum, fn.columnNum, new LinkedList<>(),
              new LinkedList<>(), fn.export, fn.required, fn.isOverride, fn.name,
              new ClassDeclType(Nullable.empty(), fn.name),
              new FunctionType(fn.fullType.within, new ReferenceType(cdecl.fullType),
                  fn.fullType.argTypes, LambdaStatus.NOT_LAMBDA),
              fn.params, Nullable.of(fnBody), fn.getIsStatic(), fn.parentCall, fn.metaType);
          tmp.setScoping(fn.getScoping());
          tmp.setOriginalLabel(fn.getOriginalLabel());
          tmp.setOrdering(fn.getOrdering());
          fn = tmp;
        } else if (fn.metaType == Function.MetaType.HASH) {
          hashFnName = fn.name;
        } else if (fn.metaType == Function.MetaType.CMP) {
          cmpFnName = fn.name;
        }

        // then transpile fn
        hppResult += indent;
        if (fn.getIsStatic()) {
          hppResult += "static ";
        }
        hppResult += transpileType(fn.fullType.returnType) + " " + fn.name + "("
            + transpileParams(fn.params) + ");\n";
        result += transpileFunction(fn, false, cdecl.name + "::") + "\n";
      } else if (cline.ast instanceof ClassDeclaration) {
        // result += indent + transpileClassDeclaration((ClassDeclaration) cline.ast);
        throw new TranspilerError("internal error: CxxTranspiler cannot handle inner classes");
      } else if (cline.ast instanceof FfiStatement) {
        hppResult += indent + transpileFfiStatement((FfiStatement) cline.ast) + "\n";
      }
    }

    // add equals function
    if (!cmpFnName.isEmpty()) {
      final String cmpEnum = transpileType(cmpResultType) + "::" + cmpEqId;
      hppResult += "bool operator==(const " + cdecl.name + "&obj) {\n" + "  return " + cmpFnName
          + "(const_cast<" + cdecl.name + "*>(&obj)) == " + cmpEnum + ";\n" + "}\n";
    }

    // add vtable stuff
    Nullable<ModuleEndpoint> modEp = project.lookupModule(program.moduleStatement.fullType,
        project.getAuditTrail());
    if (modEp.isNull()) {
      throw new TranspilerError(
          "internal error: failed to lookup module: " + program.moduleStatement.fullType);
    }
    try {
      modEp.get().load();
    } catch (FatalMessageException e) {
      throw new TranspilerError("internal error while loading module: " + e);
    }
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    // find our vtable
    ClassDeclType absName = new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name);
    ClassVtable vtable = moduleHandle.moduleBuildObj.vtables.get(absName);
    if (vtable == null) {
      throw new TranspilerError("internal error: Did not find vtable to copy for: " + absName);
    }

    int vIndex = 0;
    for (VtableEntry entry : vtable.vtableEntries) {
      hppResult += indent + "virtual " + transpileType(entry.getType().returnType) + " ";
      result += transpileType(entry.getType().returnType) + " " + cdecl.name + "::";

      String fnType = "callVtable_" + vIndex + "(";
      String fnCall = indent + INCREASE_INDENT;
      if (!(entry.getType().returnType instanceof VoidType)) {
        fnCall += "return ";
      }
      fnCall += entry.getName() + "(";
      int pIndex = 0;
      for (Type paramTy : entry.getType().argTypes) {
        if (pIndex != 0) {
          fnType += ", ";
          fnCall += ", ";
        }
        fnType += transpileType(paramTy) + " p" + pIndex;
        fnCall += "p" + pIndex;
        pIndex++;
      }
      fnType += ")";
      fnCall += ");";

      hppResult += fnType + ";\n";
      result += fnType + " {\n" + fnCall + "\n" + indent + "}\n";

      vIndex++;
    }

    indent = saveIndent;
    hppResult += "\n" + indent + "};\n";

    // if this fixes any deferred statics, add them now
    for (int idx = 0; idx < deferredStatics.size();) {
      Pair<String, String> deferred = deferredStatics.get(idx);
      if (definedTypes.contains(deferred.a)) {
        result += deferred.b;
        deferredStatics.remove(idx);
      } else {
        idx++;
      }
    }

    result += customToStrFn;
    result += customRefToStrFn;

    result += collectStaticAssignments + "\n";

    hppResult += "};\n";

    // add hashcode
    if (!hashFnName.isEmpty()) {
      final String absClass = project.dirPath() + "::" + transpileType(cdecl.fullDeclType);
      final String hashFn = "template <>                                                    \n"
          + "struct hash<" + absClass + "> {                                                \n"
          + "  std::size_t operator()(const " + absClass + "& obj) const {                  \n"
          + "    return const_cast<" + absClass + "&>(obj)." + hashFnName + "();            \n"
          + "  }                                                                            \n"
          + "};\n";
      hashFunctions.add(hashFn);
    }

    return new Pair<String, String>(result, hppResult);
  }

  private String transpileFunction(Function fn, boolean addToTop, String namePrefix)
      throws TranspilerError {
    String result = "";
    result += transpileType(fn.fullType.returnType) + " ";
    result += namePrefix + fn.name + "(";

    result += transpileParams(fn.params);
    result += ")";
    if (addToTop) {
      topLevelFuncsDecls += result + ";\n";
    }
    result += " " + transpileBlockStatement(fn.getBody().get());

    if (addToTop) {
      result = result + "\n;";
    }
    return result;
  }

  private String transpileParams(List<DeclarationStatement> params) throws TranspilerError {
    String result = "";
    boolean first = true;
    for (DeclarationStatement param : params) {
      if (!first)
        result += ", ";
      first = false;
      result += wrapIdWithType(param.name, param.type);
    }
    return result;
  }

  private String transpileMain(Program program) throws TranspilerError {
    String out = "int main(int argc, char** argv) {\n";

    Nullable<Function> main = program.getMainOrStart(Function.MetaType.START);
    if (main.isNull()) {
      return "";
    }

    String mainFn = indent + INCREASE_INDENT + "return " + main.get().name;
    if (main.get().fullType.argTypes.isEmpty()) {
      mainFn += "();";
    } else {
      // TODO pass in command line
      out += indent + "std::vector<std::string> argvVec(argc);\n";
      out += indent + "for (int n = 0; n < argc; n++) {\n";
      out += indent + INCREASE_INDENT + "argvVec[n] = std::string(argv[n]);\n";
      out += indent + "}\n";
      mainFn += "(argvVec);";
    }
    out += mainFn + "\n";
    out += "}\n";

    return out;
  }

  public static String transpileType(Type type) {
    if (!(type instanceof AbstractType)) {
      throw new IllegalArgumentException("transpileType type is not an instance of AbstractType");
    }
    String result = "";
    FunctionType fTy = null;
    ClassType uTy = null;
    ClassDeclType declTy = null;
    EnumType enumTy = null;
    EnumDeclType enumDeclTy = null;
    boolean first = true;

    AbstractType aType = (AbstractType) type;
    switch (aType.typeType) {
      case VOID_TYPE:
      case BOOL_TYPE:
      case CHAR_TYPE:
        return aType.toString().replaceAll("const ", "");
      case INT_TYPE:
        return "int";
      case FLOAT32_TYPE:
        return "float";
      case FLOAT64_TYPE:
        return "double";
      case STRING_TYPE:
        return "std::string";
      case ARRAY_TYPE:
        return "std::vector<" + transpileType(type.getInnerTypes().get(0)) + ">";
      case MAP_TYPE:
        return "std::map<" + transpileType(type.getInnerTypes().get(0)) + ", "
            + transpileType(type.getInnerTypes().get(1)) + ">";
      case FUNCTION_TYPE:
        fTy = (FunctionType) aType;
        result = "std::function<" + transpileType(fTy.returnType) + "(";
        for (Type ty : fTy.argTypes) {
          if (!first)
            result += ", ";
          first = false;
          result += transpileType(ty);
        }
        result += ")>";
        return result;
      case REFERENCE_TYPE:
        result = transpileType(((ReferenceType) aType).innerType) + "*";
        return result;
      case CLASS_DECL_TYPE:
        declTy = (ClassDeclType) aType;
        result = "";

        if (declTy.outerType.isNotNull()) {
          result += transpileType(declTy.outerType.get()) + "::";
        }

        result += declTy.name;
        return result;
      case CLASS_TYPE:
        uTy = (ClassType) aType;
        result = "";

        if (uTy.outerType.isNotNull()) {
          result += transpileType(uTy.outerType.get()) + "::";
        }

        result += uTy.name;
        if (uTy.innerTypes.size() > 0) {
          for (Type ty : uTy.innerTypes) {
            if (!first)
              result += ", ";
            first = false;
            result += transpileType(ty);
          }
        }
        return result.replace(".", "::"); // this replace is for any nested types
      case ENUM_DECL_TYPE:
        enumDeclTy = (EnumDeclType) aType;
        result = "";

        if (enumDeclTy.outerType.isNotNull()) {
          result += transpileType(enumDeclTy.outerType.get()) + "::";
        }

        result += enumDeclTy.name;
        return result;
      case ENUM_TYPE:
        enumTy = (EnumType) aType;
        result = "";

        if (enumTy.outerType.isNotNull()) {
          result += transpileType(enumTy.outerType.get()) + "::";
        }

        result += enumTy.name;
        return result;
      case MULTI_TYPE:
        return transpileMultiType((MultiType) type);
      case MODULE_TYPE:
        return transpileModuleType((ModuleType) type);
      default:
        break;
    }
    return "/*<Error: " + aType.typeType + ">*/";
  }

  public static String transpileModuleType(ModuleType type) {
    List<String> names = type.toList();
    return String.join("::", names);
  }

  public static String transpileMultiType(MultiType type) {
    String types = "<";
    boolean first = true;
    for (Type ty : type.types) {
      if (!first) {
        types += ", ";
      }
      first = false;
      types += transpileType(ty);
    }
    return "std::tuple" + types + ">";
  }

  private String transpileStatement(Statement ast) throws TranspilerError {
    if (!(ast instanceof AbstractStatement)) {
      throw new TranspilerError("transpileStatement ast is not an instance of AbstractStatement");
    }
    AbstractStatement node = (AbstractStatement) ast;
    switch (node.astType) {
      case FUNCTION:
        return transpileFunction((Function) ast, true, "") + "\n";
      case ASSIGNMENT_STATEMENT:
        return transpileAssignmentStatement((AssignmentStatement) node);
      case BLOCK_STATEMENT:
        return transpileBlockStatement((BlockStatement) node);
      case BREAK_STATEMENT:
        return "break";
      case CONDITIONAL_STATEMENT:
        return transpileConditionalStatement((ConditionalStatement) node);
      case CONTINUE_STATEMENT:
        return "continue";
      case DECLARATION_STATEMENT:
        return transpileDeclarationStatement((DeclarationStatement) node);
      case FOR_STATEMENT:
        return transpileForStatement((ForStatement) node);
      case FUNCTION_CALL_STATEMENT:
        return transpileFunctionCallExpression(
            (FunctionCallExpression) ((FunctionCallStatement) node).fnExpression);
      case RETURN_STATEMENT:
        return transpileReturnStatement((ReturnStatement) node);
      case WHILE_STATEMENT:
        return transpileWhileStatement((WhileStatement) node);
      case FFI_STATEMENT:
        return transpileFfiStatement((FfiStatement) node);
      case EXPRESSION_STATEMENT:
        return transpileExpression(((ExpressionStatement) node).expression);
      case DELETE_STATEMENT:
        return transpileDeleteStatement((DeleteStatement) node);
      default:
        break;
    }
    return "/*<Error: " + node.astType + ">*/";
  }

  private String transpileDeleteStatement(DeleteStatement node) throws TranspilerError {
    String result = "";

    if (node.target.getDeterminedType() instanceof ReferenceType) {
      return "delete " + transpileExpression(node.target);
    }

    return result;
  }

  private String transpileFfiStatement(FfiStatement node) {
    String val = node.literal.value;
    String head = "$HEADER:";
    if (val.startsWith(head)) {
      val = val.substring(head.length());
      hppImports += val + "\n";
      return "";
    }
    return val;
  }

  private String transpileWhileStatement(WhileStatement node) throws TranspilerError {
    String result = "while (" + transpileExpression(node.guard) + ") ";
    result += transpileStatement(node.body) + "\n";
    return result;
  }

  private String transpileReturnStatement(ReturnStatement node) throws TranspilerError {
    String result = "return";
    if (node.returnValue.isNotNull()) {
      result += " " + transpileExpression(node.returnValue.get());
    }
    return result;
  }

  private String transpileForStatement(ForStatement node) throws TranspilerError {
    String result = "for (";
    if (node.init.isNotNull()) {
      result += transpileStatement(node.init.get());
    }
    result += "; " + transpileExpression(node.guard) + "; ";
    if (node.action.isNotNull()) {
      result += transpileStatement(node.action.get());
    }
    result += ") " + transpileStatement(node.body);
    return result;
  }

  private String transpileDeclarationStatement(DeclarationStatement node) throws TranspilerError {
    String result = transpileType(node.type) + " " + node.name;
    if (node.getValue().isNotNull()) {
      result += " = " + transpileExpression(node.getValue().get());
    }
    return result;
  }

  private String transpileConditionalStatement(ConditionalStatement node) throws TranspilerError {
    String result = "if (" + transpileExpression(node.guard) + ") ";
    result += transpileStatement(node.thenBlock);
    if (node.elsBlock.isNotNull()) {
      result += " else " + transpileStatement(node.elsBlock.get());
    }
    return result;
  }

  private String transpileBlockStatement(BlockStatement node) throws TranspilerError {
    String result = indent + "}"; // last brace first
    String save_indent = indent;
    indent += INCREASE_INDENT;

    // iterate bottom up with option for others to insert statements
    for (int i = node.statements.size() - 1,
        j = 0; i >= 0; j++, i = node.statements.size() - 1 - j) {
      Statement statement = node.statements.get(i);

      String tmp = indent + transpileStatement(statement);
      if (!(statement instanceof ConditionalStatement) && !(statement instanceof BlockStatement)
          && !(statement instanceof FfiStatement)) {
        tmp += ";";
      }
      tmp += "\n";
      result = tmp + result;
    }

    indent = save_indent;
    result = "{\n" + result; // first brace last
    return result;
  }

  private String transpileAssignmentStatement(AssignmentStatement node) throws TranspilerError {
    return transpileLvalue(node.target) + " = " + transpileExpression(node.source);
  }

  private String transpileLvalue(Lvalue ast) throws TranspilerError {
    if (!(ast instanceof AbstractLvalue)) {
      throw new TranspilerError("transpileLvalue ast is not an instance of AbstractLvalue");
    }
    AbstractLvalue node = (AbstractLvalue) ast;
    switch (node.astType) {
      case ID_LVALUE:
        return transpileLvalueId((LvalueId) node);
      case BRACKET_LVALUE:
        return transpileLvalueBracket((LvalueBracket) node);
      case DOT_LVALUE:
        return transpileLvalueDot((LvalueDot) node);
      case DEREFERENCE_LVALUE:
        return transpileLvalueDereference((LvalueDereference) node);
      case FFI_LVALUE:
        return transpileLvalueFfi((LvalueFfi) node);
      case MULTI_LVALUE:
        return transpileMultiLvalue((MultiLvalue) node);
      default:
        break;
    }
    return "/*<Error: " + node.astType + ">*/";
  }

  private String transpileMultiLvalue(MultiLvalue node) throws TranspilerError {
    String res = "std::tie(";
    boolean first = true;
    for (Lvalue lvalue : node.lvalues) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += transpileLvalue(lvalue);
    }
    return res += ")";
  }

  private String transpileLvalueFfi(LvalueFfi node) {
    return node.literal.value;
  }

  private String transpileLvalueDereference(LvalueDereference node) throws TranspilerError {
    return "(*" + transpileExpression(node.dereferenced) + ")";
  }

  private String transpileLvalueDot(LvalueDot node) throws TranspilerError {
    String dot = ".";
    if (node.left.getDeterminedType() instanceof ReferenceType) {
      dot = "->";
    } else if (node.left.getDeterminedType() instanceof UserDeclType) {
      dot = "::";
    }
    return transpileExpression(node.left) + dot + node.id;
  }

  private String transpileLvalueBracket(LvalueBracket node) throws TranspilerError {
    String result = "";
    if (node.left.getDeterminedType() instanceof ReferenceType) {
      result += "(*(" + transpileExpression(node.left) + "))";
    } else {
      result += transpileExpression(node.left);
    }
    result += "[" + transpileExpression(node.index) + "]";
    return result;
  }

  private String transpileLvalueId(LvalueId node) throws TranspilerError {
    return node.id;
  }

  private String transpileExpression(Ast ast) throws TranspilerError {
    if (!(ast instanceof AbstractExpression)) {
      throw new TranspilerError("transpileExpression ast is not an instance of AbstractExpression");
    }
    AbstractExpression node = (AbstractExpression) ast;
    switch (node.astType) {
      case ARRAY_INIT_EXPRESSION:
        return transpileArrayInitExpression((ArrayInitExpression) node);
      case BINARY_EXPRESSION:
        return transpileBinaryExpression((BinaryExpression) node);
      case BOOL_EXPRESSION:
        return transpileBoolExpression((BoolExpression) node);
      case BRACKET_EXPRESSION:
        return transpileBracketExpression((BracketExpression) node);
      case CHAR_EXPRESSION:
        return transpileCharExpression((CharExpression) node);
      case DOT_EXPRESSION:
        return transpileDotExpression((DotExpression) node);
      case DOUBLE_EXPRESSION:
        return transpileFloatExpression((FloatExpression) node);
      case FUNCTION_CALL_EXPRESSION:
        return transpileFunctionCallExpression((FunctionCallExpression) node);
      case IDENTIFIER_EXPRESSION:
        return transpileIdentifierExpression((IdentifierExpression) node);
      case INTEGER_EXPRESSION:
        return transpileIntegerExpression((IntegerExpression) node);
      case LAMBDA_EXPRESSION:
        return transpileLambdaExpression((LambdaExpression) node);
      case LEFT_UNARY_OP_EXPRESSION:
        return transpileLeftUnaryOpExpression((LeftUnaryOpExpression) node);
      case MAP_INIT_EXPRESSION:
        return transpileMapInitExpression((MapInitExpression) node);
      case NULL_EXPRESSION:
        return "nullptr";
      case STRING_EXPRESSION:
        return "std::string(" + ((StringExpression) node).unparsed + ")";
      case REFERENCE_EXPRESSION:
        return transpileReferenceExpression((ReferenceExpression) node);
      case DEREFERENCE_EXPRESSION:
        return "(*(" + transpileExpression(((DereferenceExpression) node).dereferenced) + "))";
      case THIS_EXPRESSION:
        return "this";
      case VTABLE_ACCESS_EXPRESSION:
        return "callVtable_" + ((VtableAccessExpression) node).index;
      case NEW_CLASS_INSTANCE_EXPRESSION:
        return transpileNewClassInstanceExpression((NewClassInstanceExpression) node);
      case NEW_EXPRESSION:
        // node.value must check it's parent
        return transpileExpression(((NewExpression) node).value);
      case CAST_EXPRESSION:
        return transpileCastExpression((CastExpression) node);
      case FFI_EXPRESSION:
        return transpileFfiExpression((FfiExpression) node);
      case MULTI_EXPRESSION:
        return transpileMultiExpression((MultiExpression) node);
      case STATIC_CLASS_IDENTIFIER_EXPRESSION:
        return transpileStaticClassIdentifierExpression((StaticClassIdentifierExpression) node);
      case SIZEOF_EXPRESSION:
        return ((SizeofExpression) node).getCalculatedSize() + "";
      case INSTANCEOF_EXPRESSION:
        return transpileInstanceofExpression((InstanceofExpression) node);
      default:
        break;
    }
    return "/*<Error: " + node.astType + ">*/";
  }

  private String transpileInstanceofExpression(InstanceofExpression node) throws TranspilerError {
    String result = "";
    switch (node.operator) {
      case INSTANCEOF:
        break;
      case NOT_INSTANCEOF:
        result += "!";
        break;
      default:
        throw new TranspilerError("unknown operator: " + node.operator);
    }
    result += "(";
    result += "dynamic_cast<" + transpileType(node.right) + "*>(" + transpileExpression(node.left)
        + ") != nullptr";
    result += ")";
    return "(" + result + ")";
  }

  private String transpileStaticClassIdentifierExpression(StaticClassIdentifierExpression node) {
    return node.id.toString().replace(".", "::");
  }

  private String transpileMultiExpression(MultiExpression node) throws TranspilerError {
    String types = "<";
    String values = "(";
    boolean first = true;
    for (Expression expr : node.expressions) {
      if (!first) {
        types += ", ";
        values += ", ";
      }
      first = false;
      types += transpileType(expr.getDeterminedType());
      values += transpileExpression(expr);
    }
    return "std::tuple" + types + ">" + values + ")";
  }

  private String transpileFfiExpression(FfiExpression node) {
    return node.literal.value;
  }

  private String transpileCastExpression(CastExpression node) throws TranspilerError {
    return "static_cast<" + transpileType(node.target) + ">(" + transpileExpression(node.value)
        + ")";
  }

  private String transpileReferenceExpression(ReferenceExpression node) throws TranspilerError {
    return "(&(" + transpileExpression(node.referenced) + "))";
  }

  private String transpileNewClassInstanceExpression(NewClassInstanceExpression node)
      throws TranspilerError {
    boolean heap = (node.getParentAst() instanceof NewExpression);
    String result = "";
    if (heap) {
      result += "(new ";
    } else {
      // (*static_cast<to_ref_ty>(...))
      result += "(*static_cast<";
      result += transpileType(new ReferenceType((ReferencedType) node.getDeterminedType()));
      result += ">(";
    }
    result += transpileType(node.getName()) + "()";
    if (heap) {
      result += ")->";
    } else {
      result += ".";
    }
    // constructorCall always returns ptr
    result += transpileExpression(node.constructorCall);

    // change refStr for this instance for printing
    result += "->setRefStr(\"" + node.originalName + "\")";

    if (!heap) {
      result += "))";
    }

    return result;
  }

  private String transpileDotExpression(DotExpression node) throws TranspilerError {
    String result = transpileExpression(node.left);
    if (node.left.getDeterminedType() instanceof ReferenceType) {
      result += "->";
    } else if (node.left.getDeterminedType() instanceof UserDeclType) {
      result += "::";
    } else {
      result += ".";
    }
    return result + transpileExpression(node.right);
  }

  private String transpileMapInitExpression(MapInitExpression node) throws TranspilerError {
    String result = "{";
    boolean first = true;
    for (Entry<Expression, Expression> pair : node.pairs.entrySet()) {
      if (!first)
        result += ",\n";
      first = false;
      result += "{" + transpileExpression(pair.getKey()) + ", "
          + transpileExpression(pair.getValue()) + "}";
    }
    return result + "}";
  }

  private String transpileLeftUnaryOpExpression(LeftUnaryOpExpression node) throws TranspilerError {
    return "(" + node.operator.leftUnaryOpName + transpileExpression(node.right) + ")";
  }

  private String transpileLambdaExpression(LambdaExpression node) throws TranspilerError {
    String result = "";
    /* capture by copy */;

    // get free variables and add them as parameters, unless they are globals
    Map<String, Type> freeVars = node.findFreeVariables();
    freeVars.remove("print");
    freeVars.remove("currentUnixEpochTime");

    /*
     * [fv1, fv2](p1, p2) mutable -> rTy { //function body };
     * 
     */

    boolean first = true;
    result += "[";
    for (Entry<String, Type> fv : freeVars.entrySet()) {
      if (!first) {
        result += ", ";
      }
      first = false;
      result += fv.getKey();
    }

    result += "]("; // inner lambda with params
    first = true;
    for (DeclarationStatement param : node.params) {
      if (!first)
        result += ", ";
      first = false;
      result += wrapIdWithType(param.name, param.type);
    }
    result += ") mutable -> " + transpileType(node.returnType) + " "
        + transpileBlockStatement(node.body);

    return result;
  }

  private String transpileIntegerExpression(IntegerExpression node) {
    return node.value;
  }

  private String transpileIdentifierExpression(IdentifierExpression node) throws TranspilerError {
    if (node.id.equals("print")) {
      return "custom_print";
    }
    return node.id;
  }

  private String transpileFunctionCallExpression(FunctionCallExpression node)
      throws TranspilerError {
    String result = "";

    if (node.prefix.isNotNull()) {
      Expression prefixExpr = node.prefix.get();
      String dot = ".";

      Type prefixType = prefixExpr.getDeterminedType();

      if (prefixType instanceof ReferenceType) {
        dot = "->";
        prefixType = ((ReferenceType) prefixType).innerType;
      }
      if (prefixType instanceof UserDeclType) {
        // static - not a class instance
        dot = "::";
      }

      if ((prefixType instanceof ArrayType || prefixType instanceof MapType)
          && node.expression instanceof IdentifierExpression) {

        String name = ((IdentifierExpression) node.expression).id;
        if (name.equals("remove")) {
          return "custom_remove(" + transpileExpression(prefixExpr) + ", "
              + transpileExpression(node.arguments.get(0)) + ")";
        }
        if (name.equals("add")) {
          if (node.arguments.size() == 1) {
            name = "push_back";
          } else {
            return "custom_insert(" + transpileExpression(prefixExpr) + ", "
                + transpileExpression(node.arguments.get(0)) + ", "
                + transpileExpression(node.arguments.get(1)) + ")";
          }
        }
        if (name.equals("put")) {
          return "custom_insert(" + transpileExpression(prefixExpr) + ", "
              + transpileExpression(node.arguments.get(0)) + ", "
              + transpileExpression(node.arguments.get(1)) + ")";
        }

        String argStr = "(";
        boolean first = true;
        for (Expression arg : node.arguments) {
          if (!first) {
            argStr += ", ";
          }
          first = false;
          argStr += transpileExpression(arg);
        }

        return transpileExpression(prefixExpr) + dot + name + argStr + ")";
      }
      if (prefixType instanceof ModuleType) {
        dot = "::";
      }
      result += transpileExpression(prefixExpr) + dot;
    }

    result += transpileExpression(node.expression) + "(";

    boolean first = true;
    for (Expression arg : node.arguments) {
      if (!first)
        result += ", ";
      first = false;
      result += transpileExpression(arg); // if this arg is an array or map, it needs to be declared
                                          // first and then passed
    }
    return result + ")";
  }

  private String transpileFloatExpression(FloatExpression node) {
    return node.value;
  }

  private String transpileCharExpression(CharExpression node) {
    return node.unparsed;
  }

  private String transpileBracketExpression(BracketExpression node) throws TranspilerError {
    String result = "";
    if (node.left.getDeterminedType() instanceof ReferenceType) {
      result += "(*(" + transpileExpression(node.left) + "))";
    } else {
      result += transpileExpression(node.left);
    }
    return result + "[" + transpileExpression(node.index) + "]";
  }

  private String transpileBoolExpression(BoolExpression node) {
    return node.value ? "true" : "false";
  }

  private String transpileBinaryExpression(BinaryExpression node) throws TranspilerError {
    String op = node.operator.binOpName;
    boolean ffiConcat = false;
    if (op.equals("#")) {
      ffiConcat = true;
    }

    String result = "";
    if (!ffiConcat) {
      result += "(";
    }
    String tmp = transpileExpression(node.left);
    if (node.right.getDeterminedType().equals(StringType.Create())
        && !node.left.getDeterminedType().equals(StringType.Create()) && !ffiConcat) {
      FunctionOverloadReturn tmpOverloadName = builtinOverloads.lookupOverload(namespaces,
          CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, node.left.getDeterminedType()).get();
      Pair<String, String> newNamePair = tmpOverloadName.getNewName();
      String overloadName = newNamePair.a;
      String generatedTemplate = newNamePair.b;
      generatedTemplatesCpp += generatedTemplate;
      result += overloadName + "(" + tmp + ")";
    } else {
      result += tmp;
    }

    if (!ffiConcat) {
      result += " " + op + " ";
    }

    tmp = transpileExpression(node.right);
    if (node.left.getDeterminedType().equals(StringType.Create())
        && !node.right.getDeterminedType().equals(StringType.Create()) && !ffiConcat) {
      FunctionOverloadReturn tmpOverloadName = builtinOverloads.lookupOverload(namespaces,
          CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, node.right.getDeterminedType()).get();
      Pair<String, String> newNamePair = tmpOverloadName.getNewName();
      String overloadName = newNamePair.a;
      String generatedTemplate = newNamePair.b;
      generatedTemplatesCpp += generatedTemplate;
      result += overloadName + "(" + tmp + ")";
    } else {
      result += tmp;
    }

    if (!ffiConcat) {
      result += ")";
    }

    return result;
  }

  private String transpileArrayInitExpression(ArrayInitExpression node) throws TranspilerError {
    String result = transpileType(node.getDeterminedType()) + "({";
    boolean first = true;
    for (Expression val : node.values) {
      if (!first)
        result += ", ";
      first = false;
      result += transpileExpression(val);
    }
    return result + "})";
  }

  private String wrapIdWithType(String id, Type type) throws TranspilerError {
    String result = transpileType(type);

    if (type instanceof MapType || type instanceof ArrayType) {
      result += "&";
    }
    return result + " " + id;
  }
}
