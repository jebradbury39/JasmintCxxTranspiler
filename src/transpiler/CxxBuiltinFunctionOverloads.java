package transpiler;

import errors.FatalMessageException;
import errors.Nullable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import multifile.Project;
import transpile.BuiltinFnGen;
import transpile.BuiltinFunctionOverloads;
import transpile.BuiltinGenerator;
import typecheck.AnyType;
import typecheck.ArrayType;
import typecheck.BoolType;
import typecheck.CharType;
import typecheck.Float32Type;
import typecheck.Float64Type;
import typecheck.IntType;
import typecheck.MapType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import util.Pair;

public class CxxBuiltinFunctionOverloads extends BuiltinFunctionOverloads {

  public final Map<Type, Pair<String, Nullable<BuiltinFnGen>>> builtinTypes = new HashMap<>();

  public CxxBuiltinFunctionOverloads(BuiltinGenerator generator) {
    super(generator);
  }

  @Override
  public CxxBuiltinFunctionOverloads extend() {
    CxxBuiltinFunctionOverloads newVal = new CxxBuiltinFunctionOverloads(builtinGenerator);
    newVal.parent = Nullable.of(this);
    newVal.initBuiltinContainerTemplates();
    newVal.populateBuiltins(builtinGenerator);
    return newVal;
  }

  public void initBuiltinBasic() {
    builtinTypes.put(BoolType.Create(), new Pair<>("bool", Nullable.empty()));
    builtinTypes.put(CharType.Create(), new Pair<>("char", Nullable.empty()));
    builtinTypes.put(Float32Type.Create(), new Pair<>("float32", Nullable.empty()));
    builtinTypes.put(Float64Type.Create(), new Pair<>("float64", Nullable.empty()));
    builtinTypes.put(IntType.Create(false, 8), new Pair<>("uint8", Nullable.empty()));
    builtinTypes.put(IntType.Create(false, 16), new Pair<>("uint16", Nullable.empty()));
    builtinTypes.put(IntType.Create(false, 32), new Pair<>("uint32", Nullable.empty()));
    builtinTypes.put(IntType.Create(false, 64), new Pair<>("uint64", Nullable.empty()));
    builtinTypes.put(IntType.Create(true, 8), new Pair<>("int8", Nullable.empty()));
    builtinTypes.put(IntType.Create(true, 16), new Pair<>("int16", Nullable.empty()));
    builtinTypes.put(IntType.Create(true, 32), new Pair<>("int32", Nullable.empty()));
    builtinTypes.put(IntType.Create(true, 64), new Pair<>("int64", Nullable.empty()));
    builtinTypes.put(StringType.Create(), new Pair<>("string", Nullable.empty()));
  }

  public void initBuiltinContainerTemplates() {
    builtinTypes.put(new ArrayType(AnyType.Create()),
        new Pair<>("vector", Nullable.of(CxxBuiltinFunctionOverloads::arrayTemplate)));
    builtinTypes.put(new ReferenceType(new ArrayType(AnyType.Create())),
        new Pair<>("vector_ptr", Nullable.of(CxxBuiltinFunctionOverloads::arrayPtrTemplate)));
    builtinTypes.put(new MapType(AnyType.Create(), AnyType.Create()),
        new Pair<>("map", Nullable.of(CxxBuiltinFunctionOverloads::mapTemplate)));
    builtinTypes.put(new ReferenceType(new MapType(AnyType.Create(), AnyType.Create())),
        new Pair<>("map_ptr", Nullable.of(CxxBuiltinFunctionOverloads::mapPtrTemplate)));
  }

  ///////////////////// BuiltinFnGen stuff

  protected static Pair<String, String> arrayTemplate(BuiltinFunctionOverloads overloads,
      FunctionOverload ctx, Set<NamespaceName> usingNamespaces, List<Type> realArgTypes) {
    if (realArgTypes.size() != 1) {
      throw new IllegalArgumentException("expected a single array arg, but got: " + realArgTypes);
    }
    ArrayType arrayType = (ArrayType) realArgTypes.get(0);

    final String elementType = TranspileProgram.transpileType(arrayType.elementType);
    final String elementId = typeToIdentifier(arrayType.elementType);
    final String overloadName = ctx.overloadedName + "_vector_" + elementId;

    String result = "";

    // find the custom_to_string for our inner type
    Nullable<FunctionOverloadReturn> innerOverload = overloads.lookupOverload(usingNamespaces,
        ctx.overloadedName, arrayType.elementType);
    if (innerOverload.isNull()) {
      throw new IllegalArgumentException(
          "Failed to find overload for inner type: " + arrayType.elementType);
    }
    Pair<String, String> innerTmp = innerOverload.get().getNewName();

    result += innerTmp.b;
    final String innerOverloadName = innerTmp.a;

    result += "std::string " + overloadName + "(const std::vector<" + elementType + ">& item) {\n"
        + "   std::stringstream ss;                                                   \n"
        + "   ss << \"[\";                                                            \n"
        + "   bool first = true;                                                      \n"
        + "   for (const auto& iter : item) {                                         \n"
        + "      if (!first) ss << \", \";                                            \n"
        + "      first = false;                                                       \n"
        + "      ss << " + innerOverloadName + "(iter);                               \n"
        + "   }                                                                       \n"
        + "   ss << \"]\";                                                            \n"
        + "   return ss.str();                                                        \n"
        + "}                                                                          \n";

    return new Pair<>(overloadName, result);
  }

  protected static Pair<String, String> arrayPtrTemplate(BuiltinFunctionOverloads overloads,
      FunctionOverload ctx, Set<NamespaceName> usingNamespaces, List<Type> realArgTypes) {
    if (realArgTypes.size() != 1) {
      throw new IllegalArgumentException(
          "expected a single array ref arg, but got: " + realArgTypes);
    }
    ReferenceType arrayRef = (ReferenceType) realArgTypes.get(0);
    ArrayType arrayType = (ArrayType) arrayRef.innerType;

    final String elementType = TranspileProgram.transpileType(arrayType.elementType);
    final String elementId = typeToIdentifier(arrayType.elementType);
    final String overloadName = ctx.overloadedName + "_vector_ptr_" + elementId;

    String result = "";

    result += "std::string " + overloadName + "(const std::vector<" + elementType + ">* ptr) {\n"
        + "   if (ptr == nullptr) {                                                   \n"
        + "      return \"null\";                                                     \n"
        + "   }                                                                       \n"
        + "   return \"<reference::" + arrayType + ">\";                              \n"
        + "}                                                                          \n";

    return new Pair<>(overloadName, result);
  }

  protected static Pair<String, String> mapTemplate(BuiltinFunctionOverloads overloads,
      FunctionOverload ctx, Set<NamespaceName> usingNamespaces, List<Type> realArgTypes) {
    if (realArgTypes.size() != 1) {
      throw new IllegalArgumentException("expected a single map arg, but got: " + realArgTypes);
    }
    MapType mapType = (MapType) realArgTypes.get(0);

    final String keyType = TranspileProgram.transpileType(mapType.keyType);
    final String keyId = typeToIdentifier(mapType.keyType);
    final String valType = TranspileProgram.transpileType(mapType.valueType);
    final String valId = typeToIdentifier(mapType.valueType);
    final String overloadName = ctx.overloadedName + "_map_" + keyId + "_" + valId;

    String result = "";

    // find the custom_to_string for our key type
    Nullable<FunctionOverloadReturn> innerOverload = overloads.lookupOverload(usingNamespaces,
        ctx.overloadedName, mapType.keyType);
    if (innerOverload.isNull()) {
      throw new IllegalArgumentException(
          "Failed to find overload for map key type: " + mapType.keyType);
    }
    Pair<String, String> innerTmp = innerOverload.get().getNewName();

    result += innerTmp.b;
    final String keyOverloadName = innerTmp.a;

    // find the custom_to_string for our value type
    innerOverload = overloads.lookupOverload(usingNamespaces, ctx.overloadedName,
        mapType.valueType);
    if (innerOverload.isNull()) {
      throw new IllegalArgumentException(
          "Failed to find overload for map key type: " + mapType.keyType);
    }
    innerTmp = innerOverload.get().getNewName();

    result += innerTmp.b;
    final String valOverloadName = innerTmp.a;

    result += "std::string " + overloadName + "(const std::map<" + keyType + ", " + valType
        + ">& item) {\n"
        + "   std::stringstream ss;                                                           \n"
        + "   ss << \"{\";                                                                    \n"
        + "   bool first = true;                                                              \n"
        + "   for (const auto& iter : item) {                                                 \n"
        + "      if (!first) ss << \", \";                                                    \n"
        + "      first = false;                                                               \n"
        + "      ss << " + keyOverloadName + "(iter.first) << \": \" << " + valOverloadName
        + "(iter.second);\n"
        + "   }                                                                               \n"
        + "   ss << \"}\";                                                                    \n"
        + "   return ss.str();                                                                \n"
        + "}                                                                                  \n";

    return new Pair<>(overloadName, result);
  }

  protected static Pair<String, String> mapPtrTemplate(BuiltinFunctionOverloads overloads,
      FunctionOverload ctx, Set<NamespaceName> usingNamespaces, List<Type> realArgTypes) {
    if (realArgTypes.size() != 1) {
      throw new IllegalArgumentException("expected a single map ref arg, but got: " + realArgTypes);
    }
    ReferenceType arrayRef = (ReferenceType) realArgTypes.get(0);
    MapType mapType = (MapType) arrayRef.innerType;

    final String keyType = TranspileProgram.transpileType(mapType.keyType);
    final String keyId = typeToIdentifier(mapType.keyType);
    final String valType = TranspileProgram.transpileType(mapType.valueType);
    final String valId = typeToIdentifier(mapType.valueType);
    final String overloadName = ctx.overloadedName + "_map_ptr_" + keyId + "_" + valId;

    String result = "";

    result += "std::string " + overloadName + "(const std::map<" + keyType + ", " + valType
        + ">* ptr) {\n"
        + "   if (ptr == nullptr) {                                                   \n"
        + "      return \"null\";                                                     \n"
        + "   }                                                                       \n"
        + "   return \"<reference::" + mapType + ">\";                                \n"
        + "}                                                                          \n";

    return new Pair<>(overloadName, result);
  }

  ///////////////////////////////////////////

  private void populateBuiltins(BuiltinGenerator builtinGenerator) {
    Map<String, Set<FunctionOverload>> overloadedFunctions = new HashMap<>();
    Set<FunctionOverload> customToString = new HashSet<>();

    for (Entry<Type, Pair<String, Nullable<BuiltinFnGen>>> entry : builtinTypes.entrySet()) {
      List<Type> argTypes = new LinkedList<>();
      argTypes.add(entry.getKey()); // no inner types at this point
      FunctionOverload fn = new FunctionOverload(this,
          CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING,
          CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING + "_" + entry.getValue().a, argTypes,
          entry.getValue().b);
      customToString.add(fn);
    }

    overloadedFunctions.put(CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING, customToString);

    Namespace namespace = new Namespace(builtinGenerator.namespaceName, overloadedFunctions);
    namespaces.put(builtinGenerator.namespaceName, namespace);
  }

  @Override
  public void populate(Project project) throws FatalMessageException {
    // populate with the builtins first
    populateBuiltins(builtinGenerator);

    // then populate with types from the projects
    populateHelper(project, CxxBuiltinGenerator.OVERLOAD_CUSTOM_TO_STRING);
  }

}
