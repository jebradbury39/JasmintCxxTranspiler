# JasmintCxx

This project produces an executable which takes in a `.bson` file containing a serialized Jasmint AST and produces equivalent C++11 code in `cxx_out.cpp`.

### Building

1. Build the Jasmint library
2. Copy `jasmint.jar` into `lib/`
3. Run `ant`
4. Run `java -jar dist/jasmint-cxx.jar <filename>`

### Running the Transpiled Code

In order for the C++ code to work, several handwritten functions must be provided in a header.

Compile with `g++ -I/jsmnt_cxx_builtin/ jsmnt_cxx_builtin/builtin.cpp cxx_out.cpp`

You may need to specify the `-std=c++11` flag. Then run the produced executable.